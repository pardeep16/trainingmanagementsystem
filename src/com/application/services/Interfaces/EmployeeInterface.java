package com.application.services.Interfaces;

import com.application.Entities.Employee;

public interface EmployeeInterface {
	void addNewEmployee(Employee employee);
	void editProfile(Employee e);
	
}
