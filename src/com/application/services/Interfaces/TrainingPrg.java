package com.application.services.Interfaces;

import com.application.Entities.Employee;
import com.application.Entities.TrainingProgram;

public interface TrainingPrg {
	void enrollInTraining(TrainingProgram trainingProgram);
	void viewTrainingList();
	void postTrainingProgram(TrainingProgram trainingProgram);
}
