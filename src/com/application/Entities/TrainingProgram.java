package com.application.Entities;

import java.util.Date;

public class TrainingProgram {
	private int trainingId;
	private String trainingName;
	private Date startDate;
	private Date endDate;
	private String trainingDescription;
	private int trainerId;
	private int venueId;
	private String employeeId;
	
	
	
	
	public TrainingProgram(int trainingId, String trainingName, Date startDate,
			Date endDate, String trainingDescription, int trainerId,
			int venueId, String employeeId) {
		super();
		this.trainingId = trainingId;
		this.trainingName = trainingName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.trainingDescription = trainingDescription;
		this.trainerId = trainerId;
		this.venueId = venueId;
		this.employeeId = employeeId;
	}
	public int getTrainingId() {
		return trainingId;
	}
	public void setTrainingId(int trainingId) {
		this.trainingId = trainingId;
	}
	public String getTrainingName() {
		return trainingName;
	}
	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getTrainingDescription() {
		return trainingDescription;
	}
	public void setTrainingDescription(String trainingDescription) {
		this.trainingDescription = trainingDescription;
	}
	public int getTrainerId() {
		return trainerId;
	}
	public void setTrainerId(int trainerId) {
		this.trainerId = trainerId;
	}
	public int getVenueId() {
		return venueId;
	}
	public void setVenueId(int venueId) {
		this.venueId = venueId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	
	
	
	
	
	
	
	
}
