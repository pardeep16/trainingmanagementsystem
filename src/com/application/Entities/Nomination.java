package com.application.Entities;

public class Nomination {
	
	
	private boolean status;
	private int employeeId;
	private TrainingProgram trainingProgram;
	
	
	
	public Nomination(boolean status, int employeeId,
			TrainingProgram trainingProgram) {
		super();
		this.status = status;
		this.employeeId = employeeId;
		this.trainingProgram = trainingProgram;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public TrainingProgram getTrainingProgram() {
		return trainingProgram;
	}
	public void setTrainingProgram(TrainingProgram trainingProgram) {
		this.trainingProgram = trainingProgram;
	}

	
	
	

}
