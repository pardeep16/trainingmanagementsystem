package com.application.Entities;

public class Employee {
	private int employeeId;
	private String name;
	private String email;
	private long contact;
	private int projectManagerId;
	private String designation;
	private String projectName;
	
	
	
	
	
	
	public Employee(int employeeId, String name, String email, long contact,
			int projectManagerId, String designation, String projectName) {
		super();
		this.employeeId = employeeId;
		this.name = name;
		this.email = email;
		this.contact = contact;
		this.projectManagerId = projectManagerId;
		this.designation = designation;
		this.projectName = projectName;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getContact() {
		return contact;
	}
	public void setContact(long contact) {
		this.contact = contact;
	}
	public int getProjectManagerId() {
		return projectManagerId;
	}
	public void setProjectManagerId(int projectManagerId) {
		this.projectManagerId = projectManagerId;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	
	
	
	
	

	
}
