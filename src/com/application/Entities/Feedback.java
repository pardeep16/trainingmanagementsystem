package com.application.Entities;

public class Feedback {
	
	
	private String feedback;
	private int rating;
	private int employeeId;
	private int trainingId;
	
	
	
	
	public Feedback(String feedback, int rating, int employeeId, int trainingId) {
		super();
		this.feedback = feedback;
		this.rating = rating;
		this.employeeId = employeeId;
		this.trainingId = trainingId;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public int getTrainingId() {
		return trainingId;
	}
	public void setTrainingId(int trainingId) {
		this.trainingId = trainingId;
	}
	
	
}
