package com.application.Entities;

public class Venue {
	
	private int venueId;
	private String venueName;
	private int maxSlots;
	private String address;
	
	
	
	public Venue(int venueId, String venueName, int maxSlots, String address) {
		super();
		this.venueId = venueId;
		this.venueName = venueName;
		this.maxSlots = maxSlots;
		this.address = address;
	}
	public int getVenueId() {
		return venueId;
	}
	public void setVenueId(int venueId) {
		this.venueId = venueId;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public int getMaxSlots() {
		return maxSlots;
	}
	public void setMaxSlots(int maxSlots) {
		this.maxSlots = maxSlots;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
	
}
