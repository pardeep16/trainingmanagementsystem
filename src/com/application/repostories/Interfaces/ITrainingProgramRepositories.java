package com.application.repostories.Interfaces;

import com.application.Entities.Employee;
import com.application.Entities.TrainingProgram;

public interface ITrainingProgramRepositories {
	
	void searchTrainingPrgs();
	void addNewTrainingProgram(TrainingProgram trainingProgram);
}
